import React, {useState} from 'react';
import {
  SafeAreaView,
  Text,
  View,
  Image,
  TouchableOpacity,
  StyleSheet,
} from 'react-native';
import data from './data.json';
import _ from 'lodash';
import * as CONST from '../utils/Constants';
import Folder from '../assets/folder.svg';
import FolderOpen from '../assets/folder-open.svg';
import File from '../assets/file.svg';
import Icon from 'react-native-vector-icons/FontAwesome';

const FileTree = () => {
  return (
    <SafeAreaView>
      <View>
        <Tree data={data} />
      </View>
    </SafeAreaView>
  );
};

const Tree = ({data}) => {
  let formattedData = [];
  _.forEach(data, (item, key) => {
    item.key = key;
    formattedData.push(item);
  });

  return (
    <View>
      {formattedData.map((item) => (
        <View>
          <Node item={item} />
        </View>
      ))}
    </View>
  );
};

const Node = ({item}) => {
  const [isOpen, setIsOpen] = useState(false);
  let formattedChildData = [];

  _.forEach(item.children, (item, key) => {
    item.key = key;
    let symLinkChildren = {};
    if (item.type === CONST.SYMLINK) {
      let symlinks = item[CONST.SYMLINK_TARGET].split('\\');
      _.forEach(symlinks, (value) => {
        symLinkChildren[value] = {
          type: CONST.FOLDER,
        };
      });
      item.children = symLinkChildren;
    }
    formattedChildData.push(item);
  });
  const toggle = () => {
    setIsOpen(!isOpen);
  };

  return (
    <View style={styles.container}>
      <View style={styles.folderView}>
        <TouchableOpacity onPress={() => toggle()}>
          {item.type !== CONST.FILE && (
            <Icon
              name={isOpen ? 'caret-down' : 'caret-right'}
              size={30}
              color="#4F8EF7"
            />
          )}
        </TouchableOpacity>

        <View style={styles.treeView}>
          {isOpen ? (
            item.type === CONST.FILE ? (
              <File style={styles.fileImage} />
            ) : (
              <FolderOpen style={styles.folderImage} />
            )
          ) : item.type === CONST.FILE ? (
            <File style={styles.fileImage} />
          ) : (
            <Folder style={styles.folderImage} />
          )}
        </View>
        <Text style={styles.headerText}>{item.key}</Text>
      </View>
      {isOpen &&
        formattedChildData.map((childNode) => (
          <View>
            <Node item={childNode} />
          </View>
        ))}
    </View>
  );
};

const styles = StyleSheet.create({
  container: {marginLeft: 20, marginTop: 5},
  treeView: {flexDirection: 'row', alignItems: 'center'},
  folderView: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  fileImage: {
    width: 18,
    height: 30,
    marginLeft: 50,
    marginTop: 5,
  },
  folderImage: {
    width: 25,
    height: 30,
    marginLeft: 20,
  },
  headerText: {
    fontSize: 20,
    fontWeight: 'bold',
    paddingLeft: 5,
    color: '#4F8EF7',
  },
});

export default FileTree;
